# Danish translations of gspell.
# Copyright (C) 1999-2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the gspell package.
#
# Husk at tilføje dig i credit-listen (besked id "translator_credits")
#
# Konventioner:
#
#   plugin -> udvidelsesmodul
#   snippet -> tekststump
#
# Birger Langkjer <birger.langkjer@image.dk>, 1999.
# Kenneth Christiansen <kenneth@gnome.org>, 1999, 2000.
# Keld Simonsen <keld@dkuug.dk>, 2000, 01.
# Ole Laursen <olau@hardworking.dk>, 2002, 03, 04, 05, 06.
# Marie Lund <marielund@post.cybercity.dk>, 2004.
# Martin Willemoes Hansen <mwh@sysrq.dk>, 2004.
# M.P. Rommedahl <lhademmor@gmail.com>, 2008.
# Kenneth Nielsen <k.nielsen81@gmail.com>, 2006-2007, 2009, 2011-2012, 2014.
# Ask Hjorth Larsen <asklarsen@gmail.com>, 2007, 09, 10, 11, 13, 15.
# Alan Mortensen <alanmortensen.am@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gspell\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gspell/issues\n"
"POT-Creation-Date: 2022-09-27 19:03+0200\n"
"PO-Revision-Date: 2019-02-18 00:00+0100\n"
"Last-Translator: scootergrisen\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gspell/gspell-checker.c:415
#, c-format
msgid "Error when checking the spelling of word “%s”: %s"
msgstr "Fejl ved kontrol af stavning af ordet “%s”: %s"

#. Translators: Displayed in the "Check Spelling"
#. * dialog if there are no suggestions for the current
#. * misspelled word.
#.
#. No suggestions. Put something in the menu anyway...
#: gspell/gspell-checker-dialog.c:150 gspell/gspell-context-menu.c:217
msgid "(no suggested words)"
msgstr "(ingen foreslåede ord)"

#: gspell/gspell-checker-dialog.c:235
msgid "Error:"
msgstr "Fejl:"

#: gspell/gspell-checker-dialog.c:271
msgid "Completed spell checking"
msgstr "Stavekontrol færdig"

#: gspell/gspell-checker-dialog.c:275
msgid "No misspelled words"
msgstr "Ingen forkert stavede ord"

#. Translators: Displayed in the "Check
#. * Spelling" dialog if the current word
#. * isn't misspelled.
#.
#: gspell/gspell-checker-dialog.c:502
msgid "(correct spelling)"
msgstr "(korrekt stavning)"

#: gspell/gspell-checker-dialog.c:644
msgid "Suggestions"
msgstr "Forslag"

#: gspell/gspell-context-menu.c:152
msgid "_Language"
msgstr "_Sprog"

#: gspell/gspell-context-menu.c:240
msgid "_More…"
msgstr "_Flere …"

#. Ignore all
#: gspell/gspell-context-menu.c:285
msgid "_Ignore All"
msgstr "_Ignorér alle"

#. Add to Dictionary
#: gspell/gspell-context-menu.c:303
msgid "_Add"
msgstr "_Tilføj"

#: gspell/gspell-context-menu.c:340
msgid "_Spelling Suggestions…"
msgstr "_Staveforslag …"

#. Translators: %s is the language ISO code.
#: gspell/gspell-language.c:89
#, c-format
msgctxt "language"
msgid "Unknown (%s)"
msgstr "Ukendt (%s)"

#: gspell/gspell-language-chooser-button.c:84
msgid "No language selected"
msgstr "Der er ikke valgt et sprog"

#: gspell/gspell-navigator-text-view.c:310
msgid ""
"Spell checker error: no language set. It’s maybe because no dictionaries are "
"installed."
msgstr ""
"Stavekontrolfejl: intet sprog angivet. Måske fordi der ikke er installeret "
"nogen ordbøger."

#: gspell/resources/checker-dialog.ui:7
msgid "Check Spelling"
msgstr "Kontrollér stavning"

#: gspell/resources/checker-dialog.ui:36
msgid "Misspelled word:"
msgstr "Forkert stavet ord:"

#: gspell/resources/checker-dialog.ui:49
msgid "word"
msgstr "ord"

#: gspell/resources/checker-dialog.ui:66
msgid "Change _to:"
msgstr "Ret _til:"

#: gspell/resources/checker-dialog.ui:91
msgid "Check _Word"
msgstr "Kontrollér _ord"

#: gspell/resources/checker-dialog.ui:120
msgid "_Suggestions:"
msgstr "_Forslag:"

#: gspell/resources/checker-dialog.ui:133
msgid "_Ignore"
msgstr "_Ignorér"

#: gspell/resources/checker-dialog.ui:146
msgid "Ignore _All"
msgstr "Ignorér _alle"

#: gspell/resources/checker-dialog.ui:159
msgid "Cha_nge"
msgstr "_Ret"

#: gspell/resources/checker-dialog.ui:174
msgid "Change A_ll"
msgstr "Ret _alle"

#: gspell/resources/checker-dialog.ui:191
msgid "User dictionary:"
msgstr "Brugerordbog:"

#: gspell/resources/checker-dialog.ui:203
msgid "Add w_ord"
msgstr "Tilføj o_rd"

#: gspell/resources/language-dialog.ui:7
msgid "Set Language"
msgstr "Vælg sprog"

#: gspell/resources/language-dialog.ui:20
msgid "Select the spell checking _language."
msgstr "Vælg _sprog til stavekontrol."

#: gspell/resources/language-dialog.ui:61
msgid "_Cancel"
msgstr "_Annullér"

#: gspell/resources/language-dialog.ui:68
msgid "_Select"
msgstr "_Markér"

#, c-format
#~ msgctxt "language"
#~ msgid "%s (%s)"
#~ msgstr "%s (%s)"

#~ msgctxt "language"
#~ msgid "Default"
#~ msgstr "Forvalgt"

#~ msgid "Select the _language of the current document."
#~ msgstr "Vælg _sproget for det aktuelle dokument."
