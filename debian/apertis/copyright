Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1996-2017, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2016, Free Software
License: FSFAP

Files: Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

Files: NEWS
Copyright: no-info-found
License: LGPL

Files: aclocal.m4
 config.rpath
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 2016 Tanguy Ortolo <tanguy+debian@ortolo.eu>
License: CC0-1.0

Files: debian/tests/build
Copyright: 2019, Laurent Bigonville
 2018, 2019, Simon McVittie
 2012, Canonical Ltd.
License: CC0-1.0

Files: docs/*
Copyright: 1994-2017, Free Software Foundation, Inc.
License: GPL-3+

Files: docs/Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

Files: gspell-app/*
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

Files: gspell-app/gspell-app.c
Copyright: 2015-2017, 2020, Sébastien Wilmet <swilmet@gnome.org>
License: LGPL-2.1+

Files: gspell/*
Copyright: 2015-2017, Sébastien Wilmet
License: LGPL-2.1+

Files: gspell/Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

Files: gspell/gspell-checker-dialog.c
 gspell/gspell-checker-dialog.h
 gspell/gspell-checker.c
 gspell/gspell-checker.h
 gspell/gspell-inline-checker-text-buffer.c
 gspell/gspell-inline-checker-text-buffer.h
 gspell/gspell-language-chooser-dialog.c
 gspell/gspell-language-chooser-dialog.h
Copyright: 2015-2017, Sébastien Wilmet
 2002-2006, Paolo Maggi
License: LGPL-2.1+

Files: gspell/gspell-icu.c
 gspell/gspell-icu.h
 gspell/gspell-init.c
 gspell/gspell-language-chooser-button.c
 gspell/gspell-language-chooser-button.h
 gspell/gspell-language-chooser.c
 gspell/gspell-language-chooser.h
 gspell/gspell-navigator-text-view.c
 gspell/gspell-navigator-text-view.h
 gspell/gspell-navigator.c
 gspell/gspell-navigator.h
 gspell/gspell-text-iter.c
 gspell/gspell-text-iter.h
 gspell/gspell.h
Copyright: 2015-2017, 2020, Sébastien Wilmet <swilmet@gnome.org>
License: LGPL-2.1+

Files: gspell/gspell-language.c
 gspell/gspell-language.h
Copyright: 2015, 2016, 2020, Sébastien Wilmet
 2008, Novell, Inc.
 2006, Paolo Maggi
License: LGPL-2.1+

Files: gspell/gspell-osx.c
Copyright: 2011, 2014, Jesse van den Kieboom
License: LGPL-2.1+

Files: gspell/gspell-osx.h
 gspell/gspell-utils.c
 gspell/gspell-utils.h
Copyright: 2015-2017, Sébastien Wilmet
 2010, 2011, 2014, Jesse van den Kieboom
License: LGPL-2.1+

Files: gspell/gspellregion.c
 gspell/gspellregion.h
Copyright: 2016, Sébastien Wilmet <swilmet@gnome.org>
 2002, Gustavo Giráldez <gustavo.giraldez@gmx.net>
License: LGPL-2.1+

Files: gspell/resources/*
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

Files: gtk-doc.make
Copyright: 2003, James Henstridge
License: GPL-3+

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ and/or GPL-3+) with Libtool exception

Files: m4/*
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/ax_ac_append_to_file.m4
 m4/ax_ac_print_to_file.m4
Copyright: 2009, Allan Caffee <allan.caffee@gmail.com>
License: FSFAP

Files: m4/ax_add_am_macro_static.m4
 m4/ax_am_macros_static.m4
Copyright: 2009, Tom Howard <tomhoward@users.sf.net>
 2009, Allan Caffee <allan.caffee@gmail.com>
License: FSFAP

Files: m4/ax_append_compile_flags.m4
 m4/ax_append_link_flags.m4
Copyright: 2011, Maarten Bosmans <mkbosmans@gmail.com>
License: FSFAP

Files: m4/ax_append_flag.m4
 m4/ax_check_compile_flag.m4
 m4/ax_check_link_flag.m4
Copyright: 2011, Maarten Bosmans <mkbosmans@gmail.com>
 2008, Guido U. Draheim <guidod@gmx.de>
License: FSFAP

Files: m4/ax_check_gnu_make.m4
Copyright: 2015, Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
 2008, John Darrington <j.darrington@elvis.murdoch.edu.au>
License: FSFAP

Files: m4/ax_code_coverage.m4
Copyright: 2015, 2018, Bastien ROUCARIES
 2012, Xan Lopez
 2012, Paolo Borelli
 2012, Dan Winship
 2012, Christian Persch
 2012, 2016, Philip Withnall
License: LGPL-2.1+

Files: m4/ax_compiler_flags.m4
Copyright: 2015, David King <amigadave@amigadave.com>
 2014, 2015, Philip Withnall <philip@tecnocode.co.uk>
License: FSFAP

Files: m4/ax_compiler_flags_cflags.m4
 m4/ax_compiler_flags_ldflags.m4
Copyright: 2017, 2018, Reini Urban <rurban@cpan.org>
 2014, 2015, Philip Withnall <philip@tecnocode.co.uk>
License: FSFAP

Files: m4/ax_compiler_flags_gir.m4
 m4/ax_pkg_check_modules.m4
Copyright: 2014, 2015, Philip Withnall <philip@tecnocode.co.uk>
License: FSFAP

Files: m4/ax_file_escapes.m4
Copyright: 2008, Tom Howard <tomhoward@users.sf.net>
License: FSFAP

Files: m4/ax_is_release.m4
Copyright: 2016, Collabora Ltd.
 2015, Philip Withnall <philip@tecnocode.co.uk>
License: FSFAP

Files: m4/ax_require_defined.m4
Copyright: 2014, Mike Frysinger <vapier@gentoo.org>
License: FSFAP

Files: m4/ax_valgrind_check.m4
Copyright: 2014-2016, Philip Withnall <philip.withnall@collabora.co.uk>
License: FSFAP

Files: m4/gettext.m4
 m4/intlmacosx.m4
 m4/po.m4
 m4/progtest.m4
Copyright: 1995-2020, Free Software Foundation, Inc.
License: FSFULLR and/or GPL and/or LGPL

Files: m4/gtk-doc.m4
Copyright: 2003, James Henstridge
License: GPL-3+

Files: m4/iconv.m4
Copyright: 2000-2002, 2007-2014, 2016-2020, Free Software Foundation
License: FSFULLR

Files: m4/introspection.m4
Copyright: 2009, Johan Dahlin
License: FSFAP

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: m4/ltoptions.m4
 m4/ltsugar.m4
 m4/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: Makefile.in docs/Makefile.in gspell-app/* gspell/Makefile.in gspell/resources/* tests/Makefile.in testsuite/Makefile.in
Copyright: 2002 Gustavo Giráldez <gustavo.giraldez@gmx.net>
 2002-2006 Paolo Maggi
 2008 Novell, Inc.
 2010-2014 Jesse van den Kieboom
 2015-2016 Sébastien Wilmet
License: LGPL-2.1+

Files: m4/nls.m4
Copyright: 1995-2003, 2005, 2006, 2008-2014, 2016, 2019, 2020, Free
License: FSFULLR and/or GPL and/or LGPL

Files: m4/pkg.m4
Copyright: 2012-2015, Dan Nicholson <dbn.lists@gmail.com>
 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL-2+ with Autoconf-data exception

Files: m4/vapigen.m4
Copyright: 2012, Evan Nemerson
License: LGPL-2.1+

Files: tests/*
Copyright: 2015-2017, 2020, Sébastien Wilmet <swilmet@gnome.org>
License: LGPL-2.1+

Files: tests/Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

Files: testsuite/*
Copyright: 2015-2017, 2020, Sébastien Wilmet <swilmet@gnome.org>
License: LGPL-2.1+

Files: testsuite/Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc.
License: UNKNOWN

