gspell (1.12.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 02:15:36 +0000

gspell (1.12.0-1) unstable; urgency=medium

  * New upstream release
  * Add debian/upstream/metadata

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 12 Oct 2022 14:55:11 -0400

gspell (1.11.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Update Homepage

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 15 Jun 2022 16:29:18 -0400

gspell (1.10.0-1) unstable; urgency=medium

  * New upstream release
  * debian/libgspell-1-dev.docs: Install README.md & faq.md

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 19 Apr 2022 10:38:03 -0400

gspell (1.9.1-4) unstable; urgency=medium

  [ Laurent Bigonville ]
  * debian/rules: Fix detection whether we need to build the docs or not

  [ Jeremy Bicha ]
  * Bump debhelper-compat to 13
  * debian/rules: Simplify
  * debian/rules: Set NO_AT_BRIDGE=1 for dh_auto_test

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Wed, 09 Feb 2022 19:02:34 -0500

gspell (1.9.1-2) unstable; urgency=medium

  * debian/control.in: Move gtk-doc-tools back to the BD.
    Since 2.70, autoconf will always call gtkdocize when GTK_DOC_CHECK macro
    is used

 -- Laurent Bigonville <bigon@debian.org>  Tue, 12 Oct 2021 17:28:00 +0200

gspell (1.9.1-1) unstable; urgency=medium

  * debian/watch: Update the URL to watch for odd minor versions too
  * New upstream release
    - debian/control.in: Adjust the (build-)dependencies
    - debian/libgspell-1-2.symbols: Add newly exported symbol
  * debian/control.in: Bump Standards-Version to 4.6.0 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Tue, 12 Oct 2021 15:42:23 +0200

gspell (1.8.4-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 05:34:04 +0000

gspell (1.8.4-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 15 Sep 2020 21:17:10 +0200

gspell (1.8.3-1) unstable; urgency=medium

  * New upstream release
  * Build-Depend on dh-sequence-gir & dh-sequence-gnome
  * Drop 01_fix_dashes_test.patch: Apparently no longer needed

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 02 Feb 2020 09:47:24 -0500

gspell (1.8.2-2) unstable; urgency=medium

  * Upload to unstable

 -- Laurent Bigonville <bigon@debian.org>  Thu, 23 Jan 2020 02:11:00 +0100

gspell (1.8.2-1) experimental; urgency=medium

  * New upstream version 1.8.2 (Closes: #910574)
    - Bump (build-)dependencies for libenchant
    - Bump soname of the library to 2
  * debian/control.in: Bump Standards-Version to 4.4.1 (no further changes)
  * debian/rules: Create a writable HOME for the tests
  * Split the documentation out the -dev package and add support for the nodoc
    build-profile
  * debian/control.in: Add missing -doc packages to the build-dependencies so
    the links inside the documentation are properly resolved
  * Enable the installed tests and rework the autopkg tests to use them
  * debian/control.in: Add iso-codes to the dependencies, this seems needed to
    detect the installed languages at runtime

 -- Laurent Bigonville <bigon@debian.org>  Sat, 28 Dec 2019 16:41:33 +0100

gspell (1.6.1-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to sdk

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 23:37:57 +0000

gspell (1.6.1-2) unstable; urgency=medium

  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Add -Wl,-O1 to our LDFLAGS
  * Use debian/docs to install NEWS
  * debian/libgspell-1-1.symbols: Add Build-Depends-Package
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 20:32:33 -0500

gspell (1.6.1-1) unstable; urgency=medium

  * New upstream version 1.6.1
  * Switch to dh_missing
  * Bump Standards-Version to 4.1.1
  * Honour DEB_BUILD_OPTIONS=nocheck

 -- Michael Biebl <biebl@debian.org>  Sun, 05 Nov 2017 12:45:04 +0100

gspell (1.6.0-1) unstable; urgency=medium

  * New upstream release
  * debian/rules:
    - Use dh_install --fail-missing. Don't install sample app for now.

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 10 Sep 2017 09:57:57 -0400

gspell (1.5.4-1) unstable; urgency=medium

  * New upstream release
  * debian/libgspell-1-1.symbols: Add new symbols.
    Drop one symbol that has been marked private.
  * Refresh patch
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 30 Aug 2017 08:53:35 -0400

gspell (1.2.3-1) unstable; urgency=medium

  [ Michael Biebl ]
  * Use pristine-tar with git-buildpackage

  [ Andreas Henriksson ]
  * New upstream version 1.2.3

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 09 Mar 2017 10:49:36 +0100

gspell (1.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper compat level to 10.

 -- Michael Biebl <biebl@debian.org>  Sat, 17 Dec 2016 19:27:07 +0100

gspell (1.2.1-1) unstable; urgency=medium

  * debian/tests/build: Instead of allow-stderr, whitelist messages dbus
    prints on stderr when it is activating services.
  * New upstream release with only translation changes

 -- Iain Lane <laney@debian.org>  Mon, 14 Nov 2016 17:33:10 +0000

gspell (1.2.0-2) unstable; urgency=medium

  * debian/control.in: Make libgspell-1-1 depend against libgspell-1-common

 -- Laurent Bigonville <bigon@debian.org>  Sun, 09 Oct 2016 12:30:18 +0200

gspell (1.2.0-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * debian/rules:
    - Enable build tests with xvfb-run (LP: #1610588)
  * debian/control.in:
    - Add build dependencies needed for tests (dbus, xauth and xvfb)
  * Add basic autopkgtest to run dh_auto_test

  [ Michael Terry]
  * debian/control.in:
    - Build-depend on at-spi2-core, also needed for the tests

  [ Laurent Bigonville ]
  * New upstream release
    - debian/libgspell-1-1.symbols: Add newly exported symbols
  * Install the GObject introspection files
  * debian/control.in: Tag build-deps needed by the test with <!nocheck>
  * Add the libgspell-1-common package to ship the translation files
  * debian/patches/01_fix_dashes_test.patch: Disable part of the test, for
    some reasons it's failing with aspell (see bgo#772406)

 -- Laurent Bigonville <bigon@debian.org>  Sun, 09 Oct 2016 12:05:22 +0200

gspell (1.0.3-1) unstable; urgency=medium

  * New upstream version:
     - fix FTBFS on kfreebsd-*.
  * debian/rules: remove override no longer needed to correctly build on
    kfreebsd-*, as it was fixed upstream.

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Sat, 23 Jul 2016 19:30:08 +0200

gspell (1.0.1-3) unstable; urgency=medium

  * debian/watch: do not take into account odd-numbered minor versions.

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Fri, 03 Jun 2016 13:50:16 +0200

gspell (1.0.1-2) unstable; urgency=medium

  * debian/rules:
     - correctly generate docs/reference/gspell-1.0-sections.txt to fix FTBFS
       on kfreebsd-*.
     - remove an old and useless commented line.

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Fri, 13 May 2016 22:57:19 +0200

gspell (1.0.1-1) unstable; urgency=medium

  * New upstream version
  * debian/control: Update Standards-Version to 3.9.8 (no change needed).

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Wed, 04 May 2016 23:00:15 +0200

gspell (1.0.0-3) unstable; urgency=medium

  * Upload to unstable.
  * Add Tanguy to Uploaders.

 -- Michael Biebl <biebl@debian.org>  Sun, 17 Apr 2016 16:09:30 +0200

gspell (1.0.0-2) experimental; urgency=medium

  * debian/control: make libgspell-1-dev depend on libgspell-1-0 and devel
    libraries referrenced in gspell-1.pc (thanks to ah).

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Fri, 01 Apr 2016 18:45:47 +0200

gspell (1.0.0-1) experimental; urgency=low

  * Initial release (Closes: #816557)

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Wed, 02 Mar 2016 22:24:45 +0100
